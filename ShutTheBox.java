public class ShutTheBox {
  public static void main (String[] args){
    System.out.println("Welcome to the game.");
    Board gameBoard = new Board ();
    boolean gameOver = false;
    while (!gameOver) {
      int count = 1;
      System.out.println(gameBoard);
      System.out.println("Player 1's turn");
      boolean turn1 = gameBoard.playATurn();
      if(turn1 == true) {
        System.out.println(gameBoard);
        System.out.println("Player 2's turn");
        boolean turn2 = gameBoard.playATurn();
        if (turn2 == false) {
          count = count + 1;
          gameOver = true;
          System.out.println(gameBoard);
          System.out.println("Player "+count+" loses");
          gameOver = true;
        }
      }
    else {
      System.out.println(gameBoard);
      System.out.println("Player "+count+" loses");
      gameOver = true;
      }
    }
  }
  }

