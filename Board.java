public class Board {
  private Die dieOne;
  private Die dieTwo;
  private boolean[] closedTiles;
  
  public Board() {
    this.dieOne = new Die();
    this.dieTwo = new Die();
    this.closedTiles = new boolean[12];
    
  }
  
  public String toString() { 
    String s = "";
    for (int i=0; i < closedTiles.length;i++){
      if (closedTiles[i] == false) {
        s = s +" " +(i+1); 
      }
      else {
        s = s + " X";
      }
    }
    return s;
  }
  
  public boolean playATurn() {
    dieOne.roll();
    dieTwo.roll();
    System.out.println(dieOne);
    System.out.println(dieTwo);
    int sum = dieOne.getPips() + dieTwo.getPips();
    System.out.println(sum);
    if(closedTiles[sum-1] == true){
      closedTiles[sum-1] = true;
      System.out.println("The tile at this position is already shut.");
      return false;
    }
    else {
      closedTiles[sum-1] = true;
      System.out.println("Closing tile: " +sum);
      return true;
    }
    
  }
}